# vncsnapd
A python wrapper intended as a daemon to trigger vncsnapshot when a host is online

# STATUS:
Not yet functoional, I got distracted and moved on to something else

# TO DO: 
1. use a different way to exec then detach from vncsnapshot
1. monitor it and terminate it when it a) stops producing output or b) the target pc isn't reachable

see https://eli.thegreenplace.net/2017/interacting-with-a-long-running-child-process-in-python/
