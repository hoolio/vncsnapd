#!/home/hoolio/miniconda3/bin/python

# TO DO: 
# 1. use a different way to exec then detach from vncsnapshot
# 2. monitor it and terminate it when it a) stops producing output or b) the target pc isn't reachable
# see https://eli.thegreenplace.net/2017/interacting-with-a-long-running-child-process-in-python/

from datetime import datetime
from scapy.all import *
import time
import os

target_pc = "10.1.1.160"
retry_interval = 10
ping_duration = 1

output_rootdir = "/tmp/vncsnapd"
filename_prefix = "vncsnapd_.jpg"

fps = "10" # -fps <FPS>: Wait <FPS> seconds between snapshots
count = "9999999" # -count <COUNT>: Capture <COUNT> images

# The VNC password file to use. You can generate this
# using 'vncpasswd ' on the command line ??
password_file = "~/.vncsnapd/passwd"

def scapy_ping(hostname, timeout):
    icmp = IP(dst=hostname)/ICMP()
    #IP defines the protocol for IP addresses
    #dst is the destination IP address
    #TCP defines the protocol for the ports
    resp = sr1(icmp,timeout=timeout,verbose=0)
    if resp == None:
        return 0 # set to 1 to fake up
    else:
        return 1 # set to 0 to fake down

def logit(string):
    timestamp = datetime.today().strftime('%Y-%m-%d %H:%M:%S')
    print(str(timestamp) + " -- " + string)

def subprocessit(command_to_run, verbose):
    """
    A neat wrapper around vanilla subprocess

    takes command_to_run, excecutes it, then returns (stdout, stderr, returncode)

    TODO: consider using shlex to reduce risks of shell injection attacks

    args:
        command_to_run(string) -- the command passed to subprocess
        verbose(bool)          -- verbosity

    Returns:
        (stdout, stderr, returncode) -- a dictionary of the command outputs
    """
    if verbose:
        logit("..running "+ command_to_run)
    try:
        child = subprocess.Popen(command_to_run, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=True)
        (stdout, stderr) = child.communicate()

        # convert from byte literal
        stdout = stdout.decode('utf-8')
        stderr = stderr.decode('utf-8')

        # get rid of rubbish chars and set type
        stdout = str(stdout.rstrip())
        stderr = str(stderr.rstrip())
        returncode = int(child.returncode)

        # fail if we don't have what we think we do
        assert isinstance(returncode, int), "ERROR: returncode %s is not an integer." % returncode
        assert isinstance(stdout, str), "ERROR: stdout %s is not a str." % stdout
        assert isinstance(stderr, str), "ERROR: stderr %s is not a str." % stderr

        if verbose:
            if stdout:
                logit("....stdout:     " + stdout)
            if stderr:
                logit("....stderr:     " + stderr)
            logit("....returncode: " + returncode)

        return (stdout, stderr, returncode)

    except Exception as exception:
        logit("ERROR detected by subprocessit(), terminating")
        sys.exit(1)

def main():
    while True:
        # if the host is up                
        if scapy_ping(target_pc,ping_duration):
            logit("target " + target_pc + " is up, doing stuff")

            timestamp_as_dir = datetime.today().strftime("%Y-%m-%dT%H:%M:%S%z")

            output_full_path = os.path.join(output_rootdir, timestamp_as_dir)
            output_full_filename = os.path.join(output_full_path, filename_prefix)

            logit("..making "+ output_full_path)
            os.mkdir(output_full_path)

            # vncsnapshot -fps 10 -count 9999999 -passwd ../passwd 10.1.1.160 output.jpg
            cmd = "/usr/bin/vncsnapshot -quiet -fps " + fps + " -count " + count + " -passwd " + password_file + " " + target_pc + " " + output_full_filename

            # subprocessit will print stuff out
            (stdout, stderr, returncode) = subprocessit(cmd,True)
            # if (returncode):
            #     sys.exit(1)
 
        else:
            logit("target " + target_pc + " is down, sleeping ~" + str(retry_interval) + " seconds") 
            time.sleep(retry_interval)





main()

#BOOKMARKS
# https://dev.to/ankitdobhal/let-s-ping-the-network-with-python-scapy-5g18
# https://buildmedia.readthedocs.org/media/pdf/scapy/latest/scapy.pdf